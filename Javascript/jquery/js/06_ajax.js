'use strict'
/* 
AJAX es una tecnología que nos permite hacer peticiones "asincronas" a un servidor.
Sin necesidad de actualizar la página simplemete cargando un trozo de la web.
*/

//Utilizaremos la siguiente URL: https://reqres.in

$(document).ready(function(){
    console.log('App Works!');

    //Load
    /* $('.datos').load('https://reqres.in'); */

    //GET
    $.get("https://reqres.in/api/users", {page: 1},function(response){
        console.log(response);
        response.data.forEach((element, index) => {
            $('.datos-get').append('<ul>' + '<li>' + element.first_name + ' ' + element.last_name + ' /' + element.email+ '</li>' + '</ul>');
        });
    });

    //POST    

    $('#formulario').submit(function(e){
        e.preventDefault();
        var usuario = {
            name: $('input[name="name"]').val(),
            profesión: "programador",
            web: $('input[name="web"]').val(),
        };
        console.log(usuario);

        //Por POST
        /* $.post($(this).attr("action") ,usuario, function(response){
            console.log(response);
        }); */
        $('input').val('');

        //Por AJAX con el método AJAX
        $.ajax({
            type: "POST",
            url: $(this).attr("action"),
            data: usuario,
            beforeSend: function(){
                console.log("enviando usuarios")
            },
            success: function(response){
                console.log(response);
            },
            error: function(){
                console.log("a ocurrido un error");
            },
            timeout: 2000

        });

    });
    
});