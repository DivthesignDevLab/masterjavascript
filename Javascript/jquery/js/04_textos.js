'use strict'

$(document).ready(function(){

    $('#btn-add').removeAttr('disabled');

    $('a').each(function(index){
        var that = $(this);
        var enlace = that.attr("href");
      /*   console.log(that.attr("href")); */
        that.text(enlace);
        that.attr('target', '_blank');
    });    

    //Añadir más enlaces a la lista
    $('#btn-add').on('click', function(){
        var valor = $('#add-link');
        /* console.log(valor); */
        $('ul').append('<li><a href=http://www.' + valor.val() + '.com>' +'http://' + valor.val() + '.com' + '</li>');
        valor.val('');
    });

});