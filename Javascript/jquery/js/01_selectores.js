'use strict'

$(document).ready(function(){
    console.log("JQuery cargado y funcionando...");
    //SELECTORES
    //selector "id"
    var rojo = $('#rojo').css("background", "red")
                         .css("color", "white")
                         .css("padding", "20px");   
    /* console.log(rojo); */

    var amarillo = $("#amarillo").css("background", "yellow");

    var verde = $("#verde").css("background", "green");

    //selectores "class"
    var myClass = $(".zebra");

    var sinBorde = $(".sin-borde").on('click', function(){
        $(this).addClass("zebra");
        console.log('click')
    });
    console.log(sinBorde)

    //selectores de etiquetas
    var parrafos = $('p').css("cursor", "pointer");
    /* console.log(parrafos) */

    parrafos.on('click', function(){
        //para que la carga sea mucho más rápida y que solo accedamos al dom una vez
        var that = $(this);

        if(!that.hasClass("grande")){
            that.addClass("grande");
        }else{
            that.removeClass("grande");
        }        
    });

    //selectores de atributos
    $('[title="Google"]').css("background", "#ccc");
    $('[title="Facebook"]').css("background", "blue");

    //FIND y PARENT
    var busqueda = $('li').parent().parent().find("#rojo");
    console.log(busqueda);
});

