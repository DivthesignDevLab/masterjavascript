'use strict'

$(document).ready(function(){
    var nombre = $('#nombre');
    var datos = $('.datos');
    var caja = $('.caja');

    //FOCUS Y BLUR
    nombre.focus(function(){
        $(this).css("border", "2px solid pink");
    });

    nombre.blur(function(){
        $(this).css("border", "2px solid red");
        datos.text($(this).val()).show();
    });


    //MOUSE DOWN y MOUSE UP
    caja.mousedown(function () { 
        $(this).css("background", "red");
    });

    caja.mouseup(function () { 
        $(this).css("background", "blue");
    });

    //MOUSE MOVE
    $(document).mousemove(function(){
        var sigueme = $('.punto');
        sigueme.css("left", event.clientX)
               .css("top",  event.clientY);
    });
});