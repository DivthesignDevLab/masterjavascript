'use strict'

$(document).ready(function(){
    console.log("eventos cargados");

    //MOUSE OVER Y MOUSE OUT
    var caja = $("#caja");

    /* caja.mouseover(function(){
        $(this).css("background", "red");
    });

    caja.mouseout(function(){
        $(this).css("background", "green");
    }); */

    //HOVER
    function cambiarRojo(){
        $(this).css("background", "red");
    };

    function cambiaVerde(){
        $(this).css("background", "green");
    };

    caja.hover(cambiarRojo, cambiaVerde);

    //CLICK, DOUBLE CLICK
    caja.on('click', function(){
        $(this).css("background", "blue")
               .css("color", "white"); 
    });

    caja.on('dblclick', function(){
        $(this).css("background", "pink")
               .css("color", "white"); 
    });

    
});