'use strict'

$(document).ready(function(){
    var caja = $('.caja');
    $('#btn-show').hide()

    $('#btn-hide').on('click', function(){
        $(this).hide();
        $('#btn-show').show();
        /* $('.caja').hide('fast'); */
        caja.fadeOut();
    });

    $($('#btn-show')).on('click', function(){
        $(this).hide();
        $('#btn-hide').show();
        /* caja.show('fast'); */

        caja.fadeIn('slow');
    });

    //ANIMACIONES PERSONALIZADAS
    $('#animame').on('click', function(){
        caja.animate({
            marginLeft: '500px',
            width: '200px',
            fontSize: '25px'
        }, 'slow')
        .animate({
            borderRadius: '50%'
        });
    });

});