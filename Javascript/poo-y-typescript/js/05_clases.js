var Clothes = /** @class */ (function () {
    function Clothes() {
    }
    Clothes.prototype.setColor = function (isWhat, color, size, brand) {
        this.isWhat = isWhat;
        this.color = color;
        this.size = size;
        this.brand = brand;
    };
    ;
    Clothes.prototype.getColor = function (color) {
        return color;
    };
    ;
    return Clothes;
}());
;
var nuevaPprenda = new Clothes();
nuevaPprenda.setColor("camiseta", "blue", "L", "nike");
console.log(nuevaPprenda);
var zapatillas = new Clothes();
zapatillas.setColor("zapatillas", "red", 42, "adidas");
console.log(zapatillas);
