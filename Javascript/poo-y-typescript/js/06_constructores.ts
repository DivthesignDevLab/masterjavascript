//interfaces
/* 
es un contrato en el cual la clase a la que se le aplique debe cumplir,
defiine que metodos tienen que tener dentro.
Se utilizan para crear sofwares robustos
*/
interface playerasBase{
    setColor(color);
    getColor();
};

//DECORADORES
/* 
Es un patron de diseño que nos permite hacer una copia de esa clase 
y modificarla en función de los parámetros
*/
function estampar(logo: string){
    return function(target: Function){
        target.prototype.estampacion = function():void{
            console.log("Camiseta estampada con el logo de " + logo);
        }
    };
};
//la llamada al decorador no se cierra con punto y coma
@estampar('GUcci GAng')

class Playeras implements playerasBase{
    private color: string;
    private talla: string;
    private precio: string | number;

    //Método especial que se aplica para dar una serie de atributos iniciales al crear el objeto
    constructor(color, talla, precio){
        this.color = color;
        this.talla = talla;
        this.precio = precio;
    };
    public setColor(color){
        this.color = color;
    };
    public getColor(){
        return this.color;
    }
};



//CLASE HIJA
class Botas extends Playeras{
    public pelo: boolean;
    setPelo(pelo: boolean){
        this.pelo = pelo;
    };
    getPelo():boolean{
        return this.pelo;
    }
};

var nuevasBotas = new Botas("marrones", "39", 200);
nuevasBotas.setPelo(true);
console.log(nuevasBotas);

var nuevasPlayeras = new Playeras("violeta", "42", 80);
console.log(nuevasPlayeras);
nuevasPlayeras.setColor("pink");
console.log(nuevasPlayeras);
nuevasPlayeras.estampacion();

var nuevasPlayerasConInterface = new Playeras("blue", "52", 104);
console.log(nuevasPlayerasConInterface);
