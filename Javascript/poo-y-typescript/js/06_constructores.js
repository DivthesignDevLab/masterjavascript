var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
        return extendStatics(d, b);
    };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
;
//DECORADORES
/*
Es un patron de diseño que nos permite hacer una copia de esa clase
y modificarla en función de los parámetros
*/
function estampar(logo) {
    return function (target) {
        target.prototype.estampacion = function () {
            console.log("Camiseta estampada con el logo de " + logo);
        };
    };
}
;
//la llamada al decorador no se cierra con punto y coma
var Playeras = /** @class */ (function () {
    //Método especial que se aplica para dar una serie de atributos iniciales al crear el objeto
    function Playeras(color, talla, precio) {
        this.color = color;
        this.talla = talla;
        this.precio = precio;
    }
    ;
    Playeras.prototype.setColor = function (color) {
        this.color = color;
    };
    ;
    Playeras.prototype.getColor = function () {
        return this.color;
    };
    Playeras = __decorate([
        estampar('GUcci GAng')
    ], Playeras);
    return Playeras;
}());
;
//CLASE HIJA
var Botas = /** @class */ (function (_super) {
    __extends(Botas, _super);
    function Botas() {
        return _super !== null && _super.apply(this, arguments) || this;
    }
    Botas.prototype.setPelo = function (pelo) {
        this.pelo = pelo;
    };
    ;
    Botas.prototype.getPelo = function () {
        return this.pelo;
    };
    return Botas;
}(Playeras));
;
var nuevasBotas = new Botas("marrones", "39", 200);
nuevasBotas.setPelo(true);
console.log(nuevasBotas);
var nuevasPlayeras = new Playeras("violeta", "42", 80);
console.log(nuevasPlayeras);
nuevasPlayeras.setColor("pink");
console.log(nuevasPlayeras);
nuevasPlayeras.estampacion();
var nuevasPlayerasConInterface = new Playeras("blue", "52", 104);
console.log(nuevasPlayerasConInterface);
