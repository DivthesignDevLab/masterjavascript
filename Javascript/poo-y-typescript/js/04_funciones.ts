//FUNCIONES EN TS
/* 
En este caso le hemos dicho que el parámetro va a ser de  tipo "number"
y la funcion va a recibir un "string"
 */

function getNumero(numero: number):string{
    return "esta es la cifra: " + numero;
};

console.log(getNumero(55));