//String
var cadena: string = "esto es una cadena";

//Number
var numero: number = 9;

//Boolean
var verdadero: boolean = true;

//Any (significa cualquier cosa)
var cualquiera: any = "hola";
cualquiera = 77;

//ARRAYS
var lenguajes: Array<string> = ['PHP', 'ANGULAR', 'React'];

//ARRAYS 
var years: any[] = ["DOCE", 13, 4];


//MULTIPLES TIPOS DE DATOS
var multiple: string | number = "Hola";
multiple = 44;

//TIPOS DE DATOS PERSONALIZADOS
type letrasoNumeros = string | number | boolean;

var alfaNumerico: letrasoNumeros = true;
alfaNumerico = 55;
alfaNumerico = "hola"

console.log(cadena, numero, verdadero, cualquiera, lenguajes, years, multiple, alfaNumerico);


//AMBITO DE LAS VARIABLES EN TS
//LET vs VAR
var numero1: number = 55;
var numero2: number = 85;

if(numero1 == 55){
    let numero1 = 100;
    var numero2 = 20;
    console.log(numero1, numero2);
};

console.log(numero1, numero2);

