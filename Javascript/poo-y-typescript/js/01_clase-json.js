'use sctrict'

/* 
Una clase nos sirve como molde para luego poder aplicarlas 
*/

/* 
La Programación Orientada a Objetos nos permite tener nuestro código 
más organizado y limpio.
*/
console.log("works!")
var bicicleta = {
    color: "Rosa",
    frenos: "Shimano",
    modelo: "BMX",
    velocidadMaxima: "60km",
    cambiaColor: function(cambiar_color){
        this.color = cambiar_color;
    }
};
bicicleta.cambiaColor("Azul")
console.log(bicicleta);