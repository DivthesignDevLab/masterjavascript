//String
var cadena = "esto es una cadena";
//Number
var numero = 9;
//Boolean
var verdadero = true;
//Any (significa cualquier cosa)
var cualquiera = "hola";
cualquiera = 77;
//ARRAYS
var lenguajes = ['PHP', 'ANGULAR', 'React'];
//ARRAYS 
var years = ["DOCE", 13, 4];
//MULTIPLES TIPOS DE DATOS
var multiple = "Hola";
multiple = 44;
var alfaNumerico = true;
alfaNumerico = 55;
alfaNumerico = "hola";
console.log(cadena, numero, verdadero, cualquiera, lenguajes, years, multiple, alfaNumerico);
//AMBITO DE LAS VARIABLES EN TS
//LET vs VAR
var numero1 = 55;
var numero2 = 85;
if (numero1 == 55) {
    var numero1_1 = 100;
    var numero2 = 20;
    console.log(numero1_1, numero2);
}
;
console.log(numero1, numero2);
