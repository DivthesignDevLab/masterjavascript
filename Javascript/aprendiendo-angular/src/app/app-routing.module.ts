//importar modulos del router de @angular
import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

//importar componentes
import { ZapatillasComponent } from './zapatillas/zapatillas.component';
import { Hooks } from './hooks/hooks.component';
import { VideojuegoComponent } from './videojuego/videojuego.component';
import { HomeComponent } from './home/home.component';



const routes: Routes = [
              {path: 'home', component: HomeComponent },
              {path: 'zapatillas', component: ZapatillasComponent},
              {path: 'hooks', component: Hooks},
              {path: 'videojuegos', component: VideojuegoComponent},
              {path: '**', component: HomeComponent}
  ];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})



//exportar el modulo
export class AppRoutingModule { }
