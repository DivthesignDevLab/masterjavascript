import { Component } from '@angular/core';
import { Configuracion } from './models/configuracion';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {
  public title : string;
  public descripcion: string;
  public color: string;
  public ocultarTitulo: boolean = true;

  constructor(){
    this.title = Configuracion.titulo;
    this.descripcion = Configuracion.descripcion;
    this.color = Configuracion.color;
  }

  Metodo(){
    this.ocultarTitulo = true;
    console.log('metodo ocultar ejecutado');
    console.log(this.ocultarTitulo);
  }

  
}
