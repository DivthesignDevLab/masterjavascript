import { Component, OnInit } from '@angular/core';
import { Zapatilla } from '../models/zapatillas';


@Component({
  selector: 'app-zapatillas',
  templateUrl: './zapatillas.component.html',
  styleUrls: ['./zapatillas.component.scss']
})
export class ZapatillasComponent implements OnInit {
  public titulo: string;
  public zapatillas: Array<Zapatilla>;
  public marcas: String[];
  public color: string;
  public add_marca: string;
  public miString: string;

  constructor(){
    this.titulo = "Componente Zapatillas";
    this.marcas = new Array();
    this.zapatillas = [
      new Zapatilla('Reebook Classic', 'Reebook', 'Blanco', 80, true),
      new Zapatilla('Nike AirMax', 'Nike', 'Blanco/Rojo', 130, true),
      new Zapatilla('Adidas Campus', 'Adidas', 'Rojo', 91, true),
      new Zapatilla('Puma Bold', 'Puma', 'Negras', 39, true)
    ];
    this.color = "white"
   }

  ngOnInit() {
    console.log(this.zapatillas);
    this.getMarcas();
  }

  getMarcas(){
    this.zapatillas.forEach(
      (element) => {
        this.marcas.push(element.marca)
      })
      console.log(this.marcas)
    };
    addMarca(){
      alert(this.add_marca);
    }
    addMarcaPrint(){
      let test = this.marcas.push(this.add_marca);
      this.add_marca = '';
    }
    borrarMarca(){
      /* delete this.marcas[indice] */
      this.marcas.splice( 1);
    }
    onBlur(){
      console.log('Has salido del input')
    }
    mostrarTexto(){
        alert(this.miString);
    }

}
