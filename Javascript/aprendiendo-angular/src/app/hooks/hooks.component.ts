import { Component, OnInit, DoCheck, OnDestroy } from "@angular/core";

@Component({
    selector: 'app-hooks',
    templateUrl: './hooks.component.html',
    styleUrls: ['./hooks.component.scss']

})
export class Hooks implements OnInit, DoCheck, OnDestroy{
     public titulo: string;
    constructor(){
        this.titulo = "Hooks en Angular";
        //console.log("se ha cargado el componente Hooks!");
    }
    ngOnInit(){
        //console.log('onInit Ejecutado! ')
    }
    //se ejecuta cada vez que hay un cambio en el código
    ngDoCheck(){
        //console.log('Docheck se ejecutara cuando se produzca alguún tipo de cambio en el código')
    }

    //sirve para que se ejecute algo antes de eliminar la instancia de un componente
    ngOnDestroy(){
        //console.log('OnDestroy ejecutado');
    }
    cambiarTitulo(){
        this.titulo = "Nuevo título cambiado";
    }
}