'use strict'

//FUNCIONES ANONIMAS y CALLBACKS
//no tiene nombre y se puede meter en una variable
//se suelen meter dentro de otra función para hacer un callback
//Los "CALLBACKS" se pasan como parámetros en laas funciones

//ej función anónima
var funcionAnonima = function(nombre){
    var pelicula = "La pelicula se llama: " + nombre;
    return pelicula;
};

console.log(funcionAnonima("Los Otros"))

//ej función callback
function sumar(numero1, numero2, sumaYMuestra, sumaPorDos){
    var sumarResultados = numero1 + numero2;

    sumaYMuestra(sumarResultados);
    sumaPorDos(sumarResultados);
};


/* 
invoco la función y le paso como parámetro las dos funciones "sumaYMuestra" y "sumaPorDos", 
estas dos funciones anónimas deben recibir si o si un parámetro 
*/
sumar(3, 2, function(dato2){
    console.log("La suma es: ", dato2);

}, function(dato2){
    console.log("El  resultado al multiplicar por dos la suma es: ", (dato2 * 2));
});