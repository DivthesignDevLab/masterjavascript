'use strict'

/* 
1.Programa que pida 6 números por pantalla y los meta dentro de un array.
2.Mostrar el array entero (todos sus elementos) en el cuerpo de la página y en la consola.
3.Ordenar el array y mostralo.
4.Invertir su orden y mostrarlo.
5.Cuantos elementos tiene el array.
6.Busqueda de un valor introducido por el usuario, que nos diga si lo encuentra y su indice.
(se valorará el uso de funciones).
*/


function mostrarArray(elementos, textoCustom = ""){
    document.write("<h1>Contenido del Array "+textoCustom+"</h1>");
    document.write("<ul>");
    elementos.forEach((elemento, index) =>{
    document.write("<li>"+ elemento + "</li>");
    });
    document.write("</ul>")
};

var numeros = [];

for (let i = 0; i <= 5; i++) {
    numeros[i] = parseInt(prompt("Introduce un número", 0));
    //sepuede hacer de esta manera también con push()
    /* numeros.push(parseInt(prompt("Introduce un número", 0))); */
};

//Mostrar en el cuerpo de la página
mostrarArray(numeros);


//Ordenar y mostrar
numeros.sort();
mostrarArray(numeros, "ordenado");

//Invertir orden y mostrar
numeros.reverse();
mostrarArray(numeros, "revertido");

//contar elementos
console.log(numeros.length);

//Busqueda
var busqueda = prompt("Busca un número", 0);

var posicion = numeros.findIndex( numero => numero == busqueda);

if(posicion && posicion != -1){
    document.write("<h1>Encontrado</h1>");
    document.write("<h1>Posicion de la busqueda: "+posicion+"</h1>");
}else{
    document.write("<h1>NO encontrado</h1>");
};

console.log(numeros);