'use strict'

//ARRAYS MULTIDIMENSIONALES

var categorias = ["Terror", "comedia", "romántica", "clásico"];
var peliculas = ["la vida de Bryan", "shaft", "jurassic park"];

//array multidimensional
var cine = [categorias, peliculas];

//acceder a arrays multidimensionales
console.log(cine[0][1]);
