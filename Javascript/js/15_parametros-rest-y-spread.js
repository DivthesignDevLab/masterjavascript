'use strict'

//Parametros REST Y SPREAD
//


//REST  ...
function frutas(fruta1, fruta2, ...demásFrutas ){
    console.log("Fruta uno es: ", fruta1);
    console.log("Fruta dos es: ", fruta2);
    console.log(demásFrutas)
}

//SPREAD
var frutasPrincipales = ["coco", "manzana"]
frutas(...frutasPrincipales , "mango", "pear", "sandía", "melón");
//REST
frutas("coco", "manzana", "mango", "pear", "sandía", "melón");