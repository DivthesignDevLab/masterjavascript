'use strict'

//OPERADORES
var numero1 = 12;
var numero2 = 7;
var operacion = numero1 + numero2;
/* 
var operacion = numero1 - numero2;
var operacion = numero1 * numero2;
var operacion = numero1 / numero2;
var operacion = numero1 % numero2; 
*/

alert("El resultado de la operación es: "+ operacion);

//TIPOS DE DATOS
//numbers
var numeroEntero = 44;
console.log("esto es un NUMBER " + numeroEntero);
//string
var cadenaTexto = "Hola que tal";
console.log("Esto es un STRING " + cadenaTexto);
//booleans
var verdadero = true;
console.log("Esto es un BOOLEAN " + verdadero);
//Array
var lista = [1, 2, 3, 5];
for (let i = 0; i < lista.length; i++) {
    console.log("Esto es un ARRAY " + lista[i]);    
}
//Objeto
var persona = {
    nombre: 'Daniel',
    apellido: 'Verdu',
    edad: 33,
    profesion: 'programador'
};
console.log("Esto es un objeto "+ "Soy " + persona.nombre + " y tengo: " + persona.edad + "años");

//TYPE OF

console.log(typeof(numeroEntero));
console.log(typeof(lista));
console.log(lista instanceof Array);