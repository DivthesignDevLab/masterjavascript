'use strict'

//ARRAYS; ARREGLOS; MATRICES
//Es una coleccion de tipos de datos

var arr = ["hola", "dato1", "dato2", 52, true];

//se pueden definir también como un objeto
var lenguajes = new Array("PHP", "Go", "Js", "Java", "node", "angular");

//console.log(lenguajes[1]);
//console.log(lenguajes.length);

/* var mostrarPosicion = parseInt(prompt("introuce la posicion", 0));

if(mostrarPosicion > arr.length){
    alert("introduce un numero de 0 hasta " + arr.length);
}else{
    alert("el elemento seleccionado del Array es: "+ arr[mostrarPosicion]);
}; */

document.write("<h1>Lenguajes de programación 2019:</h1>");

document.write("<ul>")
/* for(var i = 0; i < lenguajes.length; i++){
    document.write("<li>" + lenguajes[i]+ "</li>")
}; */

//for each
lenguajes.forEach(element => {
    document.write("<li>" + element+ "</li>")
});

document.write("</ul>")