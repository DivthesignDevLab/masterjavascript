'use strict'

//ORDENAR ARRAYS
var aNumeros = ["uno", "dos", "tres", "cuatro"];

//ordenar alfabeticamente
aNumeros.sort();
console.log(aNumeros);

//darle la vuelta al array
aNumeros.reverse();
console.log(aNumeros);
