'use strict'

//FUNCIONES DENTRO DE OTRAS
//Evitar que las funciones sean muuuuyyyy largas


function hola(a, b){
    console.log(a + b);
};

function adios(a, b){
    console.log( a + b)
};

function hablar(a, b, saludar = false){
    if(saludar == false){
        adios( a + b, " Adios Don José")
    }else{
        hola(a + b , " Hola Don Pepito");
    };
    return true;
};

hablar("soy Daniel,", " y te digo... ", true)
hablar("soy Daniel,", " y te digo... ", false)