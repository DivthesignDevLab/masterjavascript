'use strict'

//Funciones de reemplazo de texto

var texto = "Hola mundo, que tal? yo programo en JavaScript";

var resultado = texto.replace("JavaScript", "Angular");//reemplazar
resultado = texto.slice(5)//recortar
resultado = texto.slice(5, 10);//recortar de a de
resultado = texto.split(" ");//array con todas las palabras separadas
resultado = texto.trim();//quita los espacios por delante y por detrás

console.log(resultado);
