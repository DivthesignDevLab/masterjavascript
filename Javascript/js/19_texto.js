'use strict'

//Transformación de textos
var numero = 444;
var texto1 = "Hola, me llamo Daniel,";
var texto2 = "soy programador"

var dato = numero.toString();
dato = texto1.toUpperCase();
dato = texto2.toLowerCase();

console.log(dato);

//calcular longitud
var nombre = "Daniel Verdú";

console.log(nombre.length);

//Concatenar
var textoTotal = texto1 + " " + texto2;
textoTotal = texto1.concat(" " + texto2);

console.log(textoTotal);
