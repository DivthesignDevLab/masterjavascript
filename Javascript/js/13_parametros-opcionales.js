'use strict'

//Parámetros opcionales
//dependiendo de el resultado nos mostrará una cosa u otra

function hablar(a, b, saludar = false){
    if(saludar == false){
        console.log(a + b + "Adios");
    }else{
        console.log(a + b +  "Hola");
    };
};

hablar("soy Daniel,", " y te digo... ", true)
hablar("soy Daniel,", " y te digo... ", false)