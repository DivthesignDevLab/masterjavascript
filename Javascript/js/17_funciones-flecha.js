'use strict'

//FUNCIONES FLECHA
/* 
es como un callback pero con esta sintaxis ()=>
*/


function sumar(numero1, numero2, sumaYMuestra, sumaPorDos){
    var sumarResultados = numero1 + numero2;

    sumaYMuestra(sumarResultados);
    sumaPorDos(sumarResultados);
};

//ej función flecha
sumar(3, 2, (dato2)=>{
    console.log("La suma es: ", dato2);

}, (dato2)=>{
    console.log("El  resultado al multiplicar por dos la suma es: ", (dato2 * 2));
});