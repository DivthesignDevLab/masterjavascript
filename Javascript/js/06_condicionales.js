'use strict'

//CONDICIONAL IF    

var edad  = 33;
var nombre = "Daniel";

if(edad >= 18){
    console.log('Hola ' + nombre + ', eres mayor de edad');

    if(edad <= 33){
        console.log("Todavía eres millenial");
    }else if(edad >= 70){
        console.log("Eres anciano");
    }else{
        console.log("Ya no eres millenial");
    };

}else{
    console.log('Hola ' + nombre + ', eres menor de edad');
};

//OPERADORES LÓGICOS

var year = 2018;

//Negación
if(year != 2016){
    console.log('el año NO es 2016');
};

//And
if(year >= 2000 && year <= 2020){
    console.log("Estamos en la era actual");
};

//Or
if(year == 2008 || year == 2018){
    console.log("El año acaba en '8'");
};