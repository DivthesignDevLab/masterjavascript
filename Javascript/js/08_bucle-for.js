'use strict'

//BUCLE FOR
//Bucle es una estructura de control que se repite un número de veces

var numeros = 100;
for (let i = 1; i <= numeros; i++) {
    console.log("Vamos por el número: " + i);    
};