'use strict'

//Bucle WHILE

var year = 2018;

while (year <= 2050) {
    console.log("Viaje al futuro: " + year);

    //sin el incremento sería infinito
    year++;
};

while (year != 1991) {
    console.log("Viaje al pasado: " + year);

    //sin el decremento sería infinito
    year--;
    if(year == 2000){
        break;
    }
};

//DO WHILE

var years = 30;
do{
    alert("Solo cuando sea diferente a 20");
    years--;
}while(years > 25);