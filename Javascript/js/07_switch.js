'use strict'

//SWITCH
var edad = 25;
var imprime = ""

switch (edad) {
    case 18:
        imprime = "Acabas de cumplir la mayoría de edad";
        break;
    case 25:
        imprime = "Eres adulto";
        break;
    case 40:
        imprime = "crisis de los 40";
        break;
    case 70:
        imprime = "Eres ya un anciano";
        break;
    default:
        imprime = "Tu edad es neutra";
        break;
};

console.log(imprime);