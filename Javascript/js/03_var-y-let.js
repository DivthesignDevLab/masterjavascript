'use strict'

//Let
//Diferencia entre var y let

//var
var numero = 40;
console.log(numero);

if(true){
    numero = 50;
    console.log(numero);
};

console.log(numero);

//let
var texto = "hola soy una variable var";
console.log(texto);

if(true){
    let texto = "hola soy una variable let";
    console.log(texto);    
};

console.log(texto);
