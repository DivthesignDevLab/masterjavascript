'use strict'

//OPERACIONES CON ARRAYS
var categorias = ["Terror", "comedia", "romántica", "clásico"];


//Añadir elementos
/* var elementos = ""
do{
    var elementos = prompt("Añade una categoria");
    categorias.push(elementos);
}while(elementos != "acabar"); */

//eliminar último elemento
/* categorias.pop() */

//eliminar elemento concreto
var indice = categorias.indexOf("romántica");
console.log(indice);

if(indice > -1){
    categorias.splice(indice, 1);
};

//convertir array a string (hay que meterlo en variable, si no no lo procesa)
var categoriasAString = categorias.join()

//convertir string a Array
var texto = "hola, dos, uno, tres, comer";
var stringAArray = texto.split(', ')

console.log(categorias);
console.log(categoriasAString);
console.log(stringAArray);