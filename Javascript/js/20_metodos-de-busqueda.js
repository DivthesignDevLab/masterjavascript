'use strict'

//Métodos de busqueda

var texto1 = "texto para buscar coincidencias, que sirve para ayudarnos, para poder ser mejor programadores"

var buscarEnTexto = texto1.indexOf('para');
buscarEnTexto = texto1.lastIndexOf('para');
buscarEnTexto = texto1.search('para');//igual que indexOf()
buscarEnTexto = texto1.match(/para/g)//expreión regular /para/g
buscarEnTexto = texto1.substr(6,4);//subStr: 6,4 -> desde la posición "6", sacame "4" letras
buscarEnTexto = texto1.charAt(6);
buscarEnTexto = texto1.startsWith("te");
buscarEnTexto = texto1.endsWith("");
buscarEnTexto = texto1.includes("sirve");//!!! key sensitive

console.log(buscarEnTexto);