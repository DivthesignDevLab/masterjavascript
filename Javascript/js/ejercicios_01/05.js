'use strict'

/* 
Muestre todos los números divisores de un número introducido en el prompt
*/

var numero = parseInt(prompt("Introduce un número", 1));

for(var i = 0; i < numero; i++){

    if(numero%i == 0){
        console.log("Divisor es: " + i);
    };
};