'use strict'

/* 
Programa que pida dos números y que nos diga cual es mayor, cual es menor 
y si son iguales.
Plus: si los números NO son un número o el valor es negativo volver a pedir los datos
*/

var mns1 = "Introduce el primer número";
var mns2 = "Introduce el segundo número";
var valor1 = ""
var valor2 = ""

var ventana1 = parseInt(prompt(mns1, valor1));
var ventana2 = parseInt(prompt(mns2, valor2));
console.log(ventana1, ventana2);

while(ventana1 <= 0 || ventana2 <= 0  || isNaN(ventana1) || isNaN(ventana2)){
    ventana1 = parseInt(prompt(mns1, valor1));
    ventana2 = parseInt(prompt(mns2, valor2));
};

if(ventana1 < ventana2){
    console.log(ventana1 + " es menor que: " + ventana2);
}else if(ventana1 == ventana2){
    console.log(ventana1 + " es igual que: " + ventana2);
}else if(ventana1 > ventana2){
    console.log(ventana1 + " es mayor que: " + ventana2);
}else{
    alert("los valores no son correctos, introduce valores correctos");
    
}